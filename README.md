# HTML and CSS Exercises


## Exercise 1: Linking CSS to HTML

### - Description: Learn how to link the external CSS file to your HTML document.

### - Task:

- Open index.html.
- Add a `<link>` tag in the `<head>` section.
- Set rel to `"stylesheet"` and href to the path of style.css.


## Exercise 2: Changing Background Color

### - Description: Change the background color of the entire page.

### - Task:

- Open style.css.
- Use the body selector.
- Set the background-color property. Try different values like red, #ff0000, rgb(255, 0, 0).


## Exercise 3: Styling Headings

### - Description: Apply styles to HTML headings (h1, h2, etc.).

### - Task:

- Target h1 in style.css.
- Change color, font-size, and text-align. 
    E.g., 
    `color: blue;, font-size: 24px;, text-align: center;`


## Exercise 4: Styling Paragraphs

### - Description: Modify the appearance of paragraph text.

### - Task:

- Use the p selector.
- Change font-family, color, and line-height. 
    E.g.,
    `font-family: Arial, sans-serif;, color: green;, line-height: 1.5;`


## Exercise 5: Styling Links

### - Description: Change how hyperlinks look on the page.

### - Tasks:

- Target a in style.css.
- Modify color, text-decoration. 
    E.g., 
    `color: purple;, text-decoration: none;`


## Exercise 6: Creating a Class Selector

### - Description: Learn to use class selectors for specific elements.

### - Tasks:

- In index.html, choose any class for suppose `class="class-name"`.
- In style.css, create a `.class-name` selector.
- Set properties like background-color and padding. 
    E.g., 
    `background-color: yellow;, padding: 5px;`


## Exercise 7: Creating an ID Selector

### - Description: Use ID selectors for unique styling.

### - Task:

- For any id you choose from html file, `id="id-name"`.
- In style.css, create a `#id-name` selector.
- Set properties like font-size and color.

## Exercise 8: Styling Lists

### - Description: Apply CSS to ordered and unordered lists.

### - Task:

- Target ul and li in style.css.
- Change list-style, margin, and padding. (Check for various values of list-style property in the `Inspect` element from the browser and then implement in the code)
